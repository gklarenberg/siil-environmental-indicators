---
title: "Sustainable Intensification Innovation Lab (SIIL) Indicators: Contextual and Environmental"
author: "Geraldine Klarenberg (gklarenberg@ufl.edu)"
date: "January 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

PI: Dr Cheryl Palm, Department of Agricultural and Biological Engineering, University of Florida, cpalm@ufl.edu

The documents associated with this project provide contextual and environmental indicators that can be used in research on sustainable intensification of agriculture. Source data and scripts that calculate indicators are also provided.
A full description of the project and the indicators is contained in the Word document "SIIL_report_env_indicators_2019FINAL.docx" in the folder "1_Documentation".

This Markdown document gives an overview of the folders and files that are available.

## Folder and file structure

The main folder "SIIL_Environmental_Indicators" contains a number of folders, this README file and an Rproject file. Always open the Rproject file before opening any of the R scripts to ensure the working environments are set properly and files can be opened.

### 1_Documentation

The Documentation folder contains the Word document "SIIL_report_env_indicators_2019FINAL.docx" which gives an overview of the project and all indicators that have been developed. Details on calculations are also given.

The document "Indicator_list.docx" gives a tabular overview of the indicators.

Two pdf documents have been developed earlier and guided much of the work done on indicator development:  
1. Guide for Sustainable Intensification Assessment Framework, 24 October 2017, by Mark Musumba, Philip Grabowski, Cheryl Palm and Sieglinde Snapp.  
2. Sustainable Intensification Assessment Methods Manual (Working Draft), 24 October 2017, by Mark Musumba, Philip Grabowski, Cheryl Palm and Sieglinde Snapp.

### 2_Data_VS_raw

This folder contains the raw data that have been obtained from the [Vital Signs project](http://www.vitalsigns.org/get-data). These data, in folder "Downloaded 07262017" were downloaded on 26 July 2017. Data used for indicator calculations were moved to separate folders, "eplot" to "weather_station".   
The original files in these folders have not been altered, and files ending with "_TZA" have only been clipped to contain data for Tanzania only. The files that end with "codebook" contain variable descriptions.

Three files had to be updated to fix a few errors or issues:  

1. In the folder "eplot", coordinates had to be fixed. The updated file is named "eplot_TZA_coordfixed.csv". The script containing the code that fixes coordinates is located in the folder "7_R_scripts" and is titled "1_Maps_landscapes_eplots.R" (lines 132-142)  

2. The folder "eplot_depth" contains soil information. As more analyzed data became available after the original download (which also contained recalculations of some problematic values), we used these data, provided by ICRAF, to do indicator calculations. The file "All_tz_vs_mir_predictions Mar 2018-PM.csv" contains this information, and "ICRAF_PredictedSoilData_metadata.csv" the variable descriptions. The old (original) data are in "eplot_depth_Oct2018_TZA.csv", and are included for completeness. To reiterate: these are NOT used for indicator calculations. *Note: there are still unresolved issues with the electric conductivity (EC) values in this file related to the magnitude of the values. Do not use these data.*  

3. The data set in the folder "household_field" needed the most extensive updates. Similar as for the eplot data, updated soil information for fields became available after the original data download. This field-level information is also contained in the aforementioned file "All_tz_vs_mir_predictions Mar 2018-PM.csv". However, this file does not contain all fields, so the information had to be combined. In addition, the original downloaded data contained duplicate rows, as some fields - but not all - were sampled twice, before and after harvest. In order to create a consistent data set, we only included the first measurements. The R scripts used to update the data set are included in this folder (as an exception). The folder contains:
    + Script "1_find_duplicates_soil_VS_032318.R" to identify duplicates in "household_field_Jan2018_TZA.csv", which creates "household_field_Jan2018_TZA_nodupl.csv"  
    + Script "2_update_field_soil_data_101718.R" to combine "household_field_Jan2018_TZA.csv" and the field data in "All_tz_vs_mir_predictions Mar 2018-PM.csv", which creates "joined_final_soil_data_Oct18.csv"
    + File "household_field codebook.csv" with variable descriptions  
    + File "household_field_Jan2018_TZA_nodupl.csv", original downloaded data for Tanzania with duplicates removed  
    + File "household_field_Jan2018_TZA.csv", original downloaded data for Tanzania  
    + File "household_field_Jan2018.csv", original downloaded data  
    + File "joined_final_soil_data_Oct18.csv", this is the final data set that is used for indicator calculations  
    *Note: there are still unresolved issues with the electric conductivity (EC) values in this file related to the magnitude of the values. Do not use these data.*

### 3_Data_shapefiles_rasters

This folder contains shapefile and raster data associated with the Vital Signs data.

### 4_Data_additional

Aside from data from the Vital Signs project, data was also gathered from other sources. Detailed descriptions of these data and their sources are given in the document "SIIL_report_env_indicators_2019FINAL.docx", only a succinct list will be given here.

a) Aridity_index_PET: Global aridity index and PET values at 30 arc second resolution, downloaded from [CGIAR](http://www.cgiar-csi.org/data/global-aridity-and-pet-database). All folders contain a readme document. The R script for AI and PET calculations also contains more details.  
b) Biomass_equations: allometric equations and wood density for the calculation of biomass. Information from two sources is available: from [GlobAllomeTree](http://globallometree.org/) and from a [wood density database](http://datadryad.org/handle/10255/dryad.235) by Zanne et al. (2009). However, wood density data are only included for completeness, they were not used in calculating biomass indicators. The R script for biomass calculations contains more details.  
c) Contextual_misc: a large number of 'miscellaneous' data sets were made available as TIF files by Markus Walsh (Earth Institute, Columbia University). The main report "SIIL_report_env_indicators_2019FINAL.docx" contains an overview. All these data are available (or transformed to) 250 m resolution.  
d) Erosivity: global erosivity values by Panagos et al. (2017) at a resolution of 30 arc seconds, downloaded from the [Joint Research Center: European Soil Data Centre (ESDAC)](https://esdac.jrc.ec.europa.eu/content/global-rainfall-erosivity).  
e) FAO_ecozones: global ecological zones as defined by FAO, downloaded from the [FAO GeoNetwork](http://www.fao.org/geonetwork/srv/en/main.home), search for “global ecological zones”, using the second edition (which includes citation info).  
f) Koppen_Geiger_TZA: global Koppen-Geiger classification at 5 arc minutes resolution, downloaded from the [Vienna University, Climate Change & Infectious Disease Group website](http://koeppen-geiger.vu-wien.ac.at/present.htm)  
g) Lookup_tables: a number of lookup tables for indicator calculations are stored in this folder. The tables and their origins are explained in the main document "SIIL_report_env_indicators_2019FINAL.docx", and in the R scripts that call them.  
h) Precipitation: global precipitation data at 0.05 degrees resolution from the Climate Hazards Group at the University of California, Santa Barbara, from the data set “Climate Hazards Group InfraRed Precipitation with Station”, [CHIRPS](http://chg.geog.ucsb.edu/data/chirps/). These monthly data from 1981 to 2016, contains precipitation in mm, anomaly values and z-scored values (more information on this in the R script that calculates indicators). Eventually only precipitation in mm was used for indicator calculation. *Note: if these data (or these folders) are not available for you when you got this project information, it is due to their size and they can be downloaded using the script "7_R_scripts/Precipitation_download.html".*

### 5_Merging_data

This folder is only available upon request from the Principal Investigator (PI), see contact details above. The information in this folder gives household coordinates and serves to link households (and their fields) to eplots. This is confidential information as per IRB guidelines.

### 6_Outputs

This folder contains all types of outputs (plots, tables, maps) per topic/folder. All outputs are created from R scripts (see next folder). We have aimed at making everything reproducible. The folder "INDICATORS" contains all indicators as outlined in the main document and the indicator list.

### 7_R_scripts

All R scripts for data cleaning and manipulation, and indicator calculation.


