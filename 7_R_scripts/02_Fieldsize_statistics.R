### Sustainable Intensification Innovation Lab (SIIL) Indicator Report
### Section: Contextual and Environmental Indicators
### PI: Dr Cheryl Palm, University of Florida (cpalm@ufl.edu)

## Sizes of fields associated with eplots
# Dr Geraldine Klarenberg
# July 2017
# University of Florida
# gklarenberg@ufl.edu
# Written using R version 3.4.3 ("Kite-Eating Tree")

# Outputs generated in this script are written to "/Outputs/Contextual_misc"

rm(list=ls(all=TRUE))  #Remove objects in memory

###### Load libraries and define directories ######
library(plyr)
library(dplyr)
library(ggplot2)
library(gridExtra)

# Define directories
output_directory <- "./6_Outputs/Contextual_misc"
rawdata_directory <- "./2_Data_VS_raw"
merging_directory <- "./5_Merging_data"

############# Read in data and select relevant columns #############
# read in field data, with field sizes
field_data<-read.csv(paste(rawdata_directory,"/household_field/household_field_Jan2018_TZA_nodupl.csv",sep=""))
# ag2a_09 is the measured field size. if not available ag2a_04 is the farmer's estimate
nrow(field_data) # 402

# read in crop data
crop_data<-read.csv(paste(rawdata_directory, "/household_field_season_fieldcrop/household_field_season_fieldcrop_TZA.csv", sep=""))
# since some fields were cultivated in both the short and the long rainy season, they're duplicated in the dataset
# find out the number of unique household-field combinations
nrow(unique(crop_data[,c('hh_refno','field_no')])) #371
# count fields for each landscape
L03_fields <- subset(crop_data, landscape_no=="L03")
nrow(unique(L03_fields[,c('hh_refno','field_no')])) #73
L10_fields <- subset(crop_data, landscape_no=="L10")
nrow(unique(L10_fields[,c('hh_refno','field_no')])) #48
L11_fields <- subset(crop_data, landscape_no=="L11")
nrow(unique(L11_fields[,c('hh_refno','field_no')])) #53
L18_fields <- subset(crop_data, landscape_no=="L18")
nrow(unique(L18_fields[,c('hh_refno','field_no')])) #33
L19_fields <- subset(crop_data, landscape_no=="L19")
nrow(unique(L19_fields[,c('hh_refno','field_no')])) #45
L20_fields <- subset(crop_data, landscape_no=="L20")
nrow(unique(L20_fields[,c('hh_refno','field_no')])) #61
L22_fields <- subset(crop_data, landscape_no=="L22")
nrow(unique(L22_fields[,c('hh_refno','field_no')])) #58

# select relevant columns
field_data_select <- subset(field_data,select=c("landscape_no","hh_refno","field_no","ag2a_09","ag2a_04"))

############# Connect households and eplots #############
############# IMPORTANT NOTE #############
# You can only run this part of the code if you have access to the (confidential) data on locations of households
# Contact the PI for more information

# load data that connects households an eplots
merge_info=read.csv(paste(merging_directory, "/VS_MergingFile.csv", sep=""))

# add eplot info 
all_data_select <- merge(field_data_select,merge_info[,c(2,4,5)],by=c("landscape_no","hh_refno"))

# set the area
all_data_select$area<-ifelse(is.na(all_data_select$ag2a_09), # use the 'measured' field area
                              all_data_select$ag2a_04, # if not available, use the farmer's estimate
                              all_data_select$ag2a_09)

# record total field areas associated with each eplot, and with each landscape
eplot_areas_all <- all_data_select %>% group_by(landscape_no,eplot_no) %>% 
  dplyr::summarize(tot_area=sum(area), ave_area=mean(area), min_area=min(area), max_area=max(area), median_area=median(area))

landscape_areas <- all_data_select %>% group_by(landscape_no) %>% 
  dplyr::summarize(tot_area=sum(area), ave_area=mean(area), min_area=min(area), max_area=max(area), median_area=median(area))

########## Write the results ###########
write.csv(eplot_areas_all,paste(output_directory, "/fieldareas_eplot.csv", sep=""), row.names=FALSE)
write.csv(landscape_areas,paste(output_directory, "/fieldareas_landscape.csv", sep=""), row.names=FALSE)

########### Plot the results ############
# plot field areas as boxplot per lanscape
eplot_areas_plot<-ggplot(eplot_areas_all,aes(x=landscape_no,y=tot_area,group=landscape_no))+
  geom_boxplot()+
  theme(axis.title.x = element_blank())+
  ylab("Total field area associated\nwith each eplot (acres)")

# plot as a bar chart
landscape_areas_plot<-ggplot(landscape_areas,aes(x=landscape_no,y=tot_area))+
  geom_bar(stat="identity")+
  ylab("Total area of fields\nwithin landscapes (acres)")+
  theme(axis.title.x = element_blank())

# put togather
all_areas_plot<-plot_grid(eplot_areas_plot,landscape_areas_plot,nrow=2)
pdf(paste(output_directory, "/field_areas.pdf", sep=""),width=7,height=7)
all_areas_plot
dev.off()
