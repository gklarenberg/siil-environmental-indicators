# Vital Signs project
# University of Florida, June 2017
# Geraldine Klarenberg

# this script calculates the following indicators:
# species richness
# Shannon entropy
# Simpson index
# at landscape and eplot level

# This script includes code developed by Conservation International
# See https://github.com/ConservationInternational/vs-indicators-calc/blob/master/Biophysical/Biophysical.R

# May 2022 - Updated to combine herb and tree biodiversity in one file, and update the column
# names so they are different (e.g. shannon_tree and shannon_herb)

rm(list=ls(all=TRUE))  #Remove objects in memory

# load libraries
library(tidyverse)
library(vegan) # biodiversity

################################# FUNCTIONS FOR BIODIVERSITY #################################  
# Shannon entropy 
shannon_index <- function(vect, base = exp(1)){ # takes a vector as input argument and set base to e
  vect <- vect[!is.na(vect)] # remove NAs from vector
  tb <- table(vect) # turns it into a table of counts
  Sh=diversity(tb, index="shannon") # calculate Shannon div (vegan package)
  return(Sh)
}

# Simpson's Index
simpson_index <- function(vect, base=exp(1)){
  vect <- vect[!is.na(vect)] # remove NAs
  if(length(vect)==0){   # if there is no data left, set vector to 0. This will give
    vect = 0
  }
  tb <- table(vect) # turns it into a table of counts
  Si=diversity(tb, index="simpson")
  return(Si)
}

# species richness
richness <- function(vect, base=exp(1)){
  vect <- vect[!is.na(vect)] # remove NAs
  tb <- table(vect) # turns it into a table of counts
  ri=specnumber(tb)
  return(ri)
}

# Function to draw and replace - taking "frequency" into account: in our case the size of the
# subplots. Result of this function is used in the next function to calculate biodiversity
random_round <- function(vect, freq){
  n <- length(vect)*freq # multiply species vector with frequency (ratio: based on sampled area)
  remainder <- n%%1 # take the decimals from n
  true <- rep(TRUE, remainder*1000) # make a vector with true
  false <- rep(FALSE, (1-remainder)*1000) # make a vector with false
  value <- sample(c(true, false), 1) # select true or false randomly, used to round n up or down
  if (value){
    n <- ceiling(n)
  }else{
    n <- floor(n)
  }
  return(sample(vect, n)) # create a sample of n rows: the smaller the ratio, the fewer species are selected
}

# function to do bootstraps to get average biodiversity
bootstrap_diversity_shannon <- function(vector, freq, iter){
  freq <- unique(freq)
  ents <- NULL
  for (j in 1:iter){
    samp <- random_round(vector, freq)
    ents <- c(ents, shannon_index(samp))
  }
  return(mean(ents))
}

bootstrap_diversity_simpson <- function(vector, freq, iter){
  freq <- unique(freq)
  ents <- NULL
  for (j in 1:iter){
    samp <- random_round(vector, freq)
    ents <- c(ents, simpson_index(samp)) 
  }
  return(mean(ents))
}

bootstrap_diversity_richness <- function(vector, freq, iter){
  freq <- unique(freq)
  ents <- NULL
  for (j in 1:iter){
    samp <- random_round(vector, freq)
    ents <- c(ents, richness(samp))
  }
  return(mean(ents))
}
##################################### TREE BIODIVERSITY #####################################

# Species are recorded for all woody stems (height > 0.5 m, dbh > 50 mm)
# recorded: number of trees in a subplot, genus, species, dbh (circ), number of stems, height, canopy width

# Biodiversity indicators for wood per eplot

# not all subplots are the same size, this is taken into account with bootstrapping
con <- read_csv("2_Data_VS_raw/eplot/eplot_TZA_coordfixed.csv")
eplot <- con %>% 
  select(id, country, landscape_no, eplot_no, field_radius) 
# calculate area and ratios
radii <- unique(eplot$field_radius) # filter unique radii
area <- pi*unique(eplot$field_radius)^2 # calculate associated area with each radii
ratio <- pi*2^2/area # ratio compared to the smallest 2 m radius. So if the radius is 2, the ratio is 1 and all observations are included.
# put this data together in one dataframe
subplot <- data.frame(field_radius=radii, area, ratio)
# each eplot will now have a ratio associated with it
eplot <- left_join(eplot, subplot)

con2 <- read_csv("2_Data_VS_raw/eplot_subplot_tree/eplot_subplot_tree_TZA.csv")
# since id's are not the same between the eplot and species file, use landscape_no and eplot_no to merge
species <- con2 %>% 
  select(country, landscape_no, eplot_no, genus, species)
species <- left_join(species, eplot, by=c("landscape_no","eplot_no"))

# every eplot has a species vector associated with it, and a ratio
species_sum1 <- species %>% group_by(landscape_no,eplot_no) %>% 
  dplyr::summarize(Shannon_tree = bootstrap_diversity_shannon(vect = paste0(genus, species), 
                                                         freq = unique(ratio),
                                                         iter = 500))
# paste0 puts the names together as one string, to ensure we look at all unique names
# summarize makes an output vector or dataframe (adding to it while calculating)
species_sum2 <- species %>% group_by(landscape_no,eplot_no) %>% 
  dplyr::summarize(Simpson_tree = bootstrap_diversity_simpson(vect = paste0(genus, species), 
                                                         freq = unique(ratio), 
                                                         iter = 500))
species_sum3 <- species %>% group_by(landscape_no,eplot_no) %>% 
  dplyr::summarize(richness_tree = bootstrap_diversity_richness(vect = paste0(genus, species), 
                                                           freq = unique(ratio), 
                                                           iter = 500))

species_sum1 <- as.data.frame(species_sum1)
species_sum2 <- as.data.frame(species_sum2)
species_sum3 <- as.data.frame(species_sum3)
species_sum <- full_join(species_sum1,species_sum2,by=c("landscape_no","eplot_no"))
species_sum <- full_join(species_sum,species_sum3,by=c("landscape_no","eplot_no"))

species_sum <- species_sum %>% 
  mutate(land_eplot = str_c("L", landscape_no, "_", eplot_no)) %>% 
  mutate(land_eplot = str_replace(land_eplot, pattern = "LL", replacement = "L"))

write_csv(species_sum,"6_Outputs/Biodiversity/biodiversity_tree_eplot.csv")

##################################### HERBACEOUS BIODIVERSITY #####################################
# Herbaceous data - Dry Weight Ranking
# The procedure is to give the species 8 points for ranking first, 2.4 points for 
# second and 1 point for third, summing for each species over all the quadrats, and 
# then summing the subtotals for each species. The percentage contribution by a species 
# to the dry mass is given by the species subtotal/grand total x 100.

####### STILL DO TO #########
# Organize "vegetation_DWR_calc.R" and put in correct folder

# Read in data
# Use the DWR calculations that were done separately ("vegetation_DWR_calc.R")
herb_data <- read_csv("6_Outputs/Biodiversity/DWR_herb_eplot.csv")
# column DWR gives the proportion of 'species points' from 'total point'

herb_species_Shannon <- herb_data %>% group_by(landscape_no,eplot_no) %>%
  dplyr::summarize(Shannon_herb = diversity(DWR,index="shannon"))

herb_species_Simpson <- herb_data %>% group_by(landscape_no,eplot_no) %>%
  dplyr::summarize(Simpson_herb = diversity(DWR,index="simpson"))

herb_species_sr <- herb_data %>% group_by(landscape_no,eplot_no) %>%
  dplyr::summarize(richness_herb = specnumber(DWR))

all_herb_species <- left_join(herb_species_Shannon,herb_species_Simpson, by=c("landscape_no","eplot_no"))
all_herb_species <- left_join(all_herb_species,herb_species_sr, by=c("landscape_no","eplot_no"))

# Add id column that combines landscape and eplot
all_herb_species <- all_herb_species %>% 
  mutate(land_eplot = str_c("L", landscape_no, "_", eplot_no)) %>% 
  mutate(land_eplot = str_replace(land_eplot, pattern = "LL", replacement = "L"))

write_csv(all_herb_species, "6_Outputs/Biodiversity/biodiversity_herb_eplot.csv")

##### Combine all biodiversity data in one file:
all_biodiv <- full_join(species_sum, all_herb_species)
write_csv(all_biodiv, "6_Outputs/Biodiversity/biodiversity_ALL_eplot.csv")


