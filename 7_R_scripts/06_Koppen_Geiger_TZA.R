# Geraldine Klarenberg
# University of Florida, 2017
# This code is adjusted from the original code that came with the Koppen_Geiger product ("Map_KG-Global_original.R")

###########################################################################################
##
## R source code to read and visualize Köppen-Geiger fields (Version of 26 January 2017)                                                                                    
##
## Climate classification after Kottek et al. (2006), downscaling after Rubel et al. (2017)
##
## Kottek, M., J. Grieser, C. Beck, B. Rudolf, and F. Rubel, 2006: World Map of the  
## Köppen-Geiger climate classification updated. Meteorol. Z., 15, 259-263.
##
## Rubel, F., K. Brugger, K. Haslinger, and I. Auer, 2017: The climate of the 
## European Alps: Shift of very high resolution Köppen-Geiger climate zones 1800-2100. 
## Meteorol. Z., DOI 10.1127/metz/2016/0816.
##
## (C) Climate Change & Infectious Diseases Group, Institute for Veterinary Public Health
##     Vetmeduni Vienna, Austria
##
###########################################################################################
# if not yet installed, run packages installations
# install.packages("raster")
# install.packages("rasterVis")
# install.packages("rworldxtra")
# install.packages("rgdal")
# install.packages("plyr")

# required packages 
library(raster) # for reasing the data
library(rasterVis) # for plotting
library(rworldxtra) # to get TZA borders
data(countriesHigh) # to get TZA borders
library(rgdal) # for working with geospatial data
library(dplyr) # for data manipulation

# Define directories
file_directory <- "./4_Data_additional/Koppen_Geiger_TZA/Map_KG-Global"
output_directory <- "./6_Outputs/Koppen_Tanzania"
spatial_directory <- "./3_Data_shapefiles_rasters" # Folder with shapefiles and rasters
rawdata_directory <- "./2_Data_VS_raw"

####### Load data ####### 
# Read raster files
period='1986-2010'
r <- raster(paste(file_directory, '/KG_', period, '.grd', sep=''))

# Color palette for climate classification
climate.colors=c("#960000", "#FF0000", "#FF6E6E", "#FFCCCC", "#CC8D14", "#CCAA54", "#FFCC00", "#FFFF64", "#007800", "#005000", "#003200", "#96FF00", "#00D700", "#00AA00", "#BEBE00", "#8C8C00", "#5A5A00", "#550055", "#820082", "#C800C8", "#FF6EFF", "#646464", "#8C8C8C", "#BEBEBE", "#E6E6E6", "#6E28B4", "#B464FA", "#C89BFA", "#C8C8FF", "#6496FF", "#64FFFF", "#F5FFFF")

# Legend must correspond to all climate classes, insert placeholders
r0 <- r[1:32]; r[1:32] <- seq(1,32,1)

# Converts raster field to categorical data
r <- ratify(r); rat <- levels(r)[[1]]

# Legend is always drawn in alphabetic order
rat$climate <- c('Af', 'Am', 'As', 'Aw', 'BSh', 'BSk', 'BWh', 'BWk', 'Cfa', 'Cfb','Cfc', 'Csa', 'Csb', 'Csc', 'Cwa','Cwb', 'Cwc', 'Dfa', 'Dfb', 'Dfc','Dfd', 'Dsa', 'Dsb', 'Dsc', 'Dsd','Dwa', 'Dwb', 'Dwc', 'Dwd', 'EF','ET', 'Ocean')

# Remove the placeholders
r[1:32] <- r0; levels(r) <- rat

####### Map Tanzania ####### 		
# Select region (Tanzania, landscapes)
# Tanzania (the loaded dataset countriesHigh has all country boundaries, which will also be cropped appropriately)
cropped <- crop(r, extent(29.06, 40.03, -11.73, -0.88)) # xmin,xmax,ymin,ymax so long then lat

####### Map landscapes and extract data ####### 
# load landsacpes, select only TZA landscapes
comm.poly <- readOGR(dsn=path.expand(paste(spatial_directory, "/VitalSignsLandscapes2/VitalSignsLandscapes.shp", sep="")), layer="VitalSignsLandscapes")
# get only the TZA landscapes
comm.poly<-comm.poly[comm.poly$country == "TZA",]
# remove L02
comm.poly<-comm.poly[-5,]
# check projections
proj4string(comm.poly)
proj4string(r) # looks to be the same, but reproject to be on the safe side

# Shapefile reprojection
comm.poly <- spTransform(comm.poly, crs(r))

# Visualization of Koppen-Geiger, Tanzania and landscapes		
if(.Platform$OS.type=="windows") {quartz<-function(...) windows(...)}
quartz(width=13, height=10, dpi=100)

print(levelplot(cropped, col.regions=climate.colors, xlab="", ylab="", 
                scales=list(x=list(limits=c(xmin(cropped), xmax(cropped)), at=seq(xmin(cropped), xmax(cropped), 5)), 
                            y=list(limits=c(ymin(cropped), ymax(cropped)), at=seq(ymin(cropped), ymax(cropped), 5))), 
                colorkey=list(space="top", tck=0, maxpixels=ncell(cropped))) +
        latticeExtra::layer(sp.polygons(countriesHigh, lwd=2)) +
        latticeExtra::layer(sp.polygons(comm.poly, lwd=2)))
# with the above packages loaded 'layer' is obscured by some other package, so you need to specify it's from latticeExtra

out=paste(output_directory, '/Tanzania_KG_1986-2010_5m.pdf', sep="")
dev.copy2pdf(file=out)


# Map landscapes separately and extract data
# for the visualization, use xmin,xmax,ymin and ymax, which are lower_le_1, lower_ri_1, lower_righ and upper_righ 
L03=comm.poly[comm.poly$landscape_=="L03",]
L10=comm.poly[comm.poly$landscape_=="L10",]
L11=comm.poly[comm.poly$landscape_=="L11",]
L18=comm.poly[comm.poly$landscape_=="L18",]
L19=comm.poly[comm.poly$landscape_=="L19",]
L20=comm.poly[comm.poly$landscape_=="L20",]
L22=comm.poly[comm.poly$landscape_=="L22",] # seems to be not a proper square
# extent is now also listed as bbox

# make maps, but also save the values
# these are stored in 'values' as a number, which as an ID is connected a climate name
# ID and climate data are give in 'levels(r)'

allvalues=extract(r, comm.poly)
allvalues[[4]]=mean(allvalues[[4]]) # for some reason L22 (which is in place #4) has 2 values, not a proper square? But both values are 4
mydata <- data.frame(coordinates(comm.poly),
                   comm.poly$landscape_, unlist(allvalues))
names(mydata) <- c("x", "y", "landscape_no", "ID")

mydata<-join(mydata,as.data.frame(levels(r)),by='ID')

write.csv(mydata,paste(output_directory,"/KoppenGeiger_landscape.csv", sep=""), row.names = FALSE)

#### Make maps - these are currently very ugly. Also L22 is a rectangle?
# Create landscape polygons/data
cropped_L03 <- crop(r, extent(L03@bbox))
cropped_L10 <- crop(r, extent(L10@bbox))
cropped_L11 <- crop(r, extent(L11@bbox))
cropped_L18 <- crop(r, extent(L18@bbox))
cropped_L19 <- crop(r, extent(L19@bbox))
cropped_L20 <- crop(r, extent(L20@bbox))
cropped_L22 <- crop(r, extent(L22@bbox))

L03_plot <- levelplot(cropped_L03, col.regions=climate.colors, xlab="", ylab="", 
                            scales=list(x=list(limits=c(xmin(cropped_L03), xmax(cropped_L03)), at=seq(xmin(cropped_L03), xmax(cropped_L03), 5)), 
                                        y=list(limits=c(ymin(cropped_L03), ymax(cropped_L03)), at=seq(ymin(cropped_L03), ymax(cropped_L03), 5))), 
                      colorkey=list(space="top", tck=0, maxpixels=ncell(cropped_L03)))
L10_plot <- levelplot(cropped_L10, col.regions=climate.colors, xlab="", ylab="", 
                            scales=list(x=list(limits=c(xmin(cropped_L10), xmax(cropped_L10)), at=seq(xmin(cropped_L10), xmax(cropped_L10), 5)), 
                                        y=list(limits=c(ymin(cropped_L10), ymax(cropped_L10)), at=seq(ymin(cropped_L10), ymax(cropped_L10), 5))),
                      colorkey=FALSE)
L11_plot <- levelplot(cropped_L11, col.regions=climate.colors, xlab="", ylab="", 
                      scales=list(x=list(limits=c(xmin(cropped_L11), xmax(cropped_L11)), at=seq(xmin(cropped_L11), xmax(cropped_L11), 5)), 
                                  y=list(limits=c(ymin(cropped_L11), ymax(cropped_L11)), at=seq(ymin(cropped_L11), ymax(cropped_L11), 5))),
                      colorkey=FALSE)
L18_plot <- levelplot(cropped_L18, col.regions=climate.colors, xlab="", ylab="", 
                      scales=list(x=list(limits=c(xmin(cropped_L18), xmax(cropped_L18)), at=seq(xmin(cropped_L18), xmax(cropped_L18), 5)), 
                                  y=list(limits=c(ymin(cropped_L18), ymax(cropped_L18)), at=seq(ymin(cropped_L18), ymax(cropped_L18), 5))),
                      colorkey=FALSE)
L19_plot <- levelplot(cropped_L19, col.regions=climate.colors, xlab="", ylab="", 
                      scales=list(x=list(limits=c(xmin(cropped_L19), xmax(cropped_L19)), at=seq(xmin(cropped_L19), xmax(cropped_L19), 5)), 
                                  y=list(limits=c(ymin(cropped_L19), ymax(cropped_L19)), at=seq(ymin(cropped_L19), ymax(cropped_L19), 5))),
                      colorkey=FALSE)
L20_plot <- levelplot(cropped_L20, col.regions=climate.colors, xlab="", ylab="", 
                      scales=list(x=list(limits=c(xmin(cropped_L20), xmax(cropped_L20)), at=seq(xmin(cropped_L20), xmax(cropped_L20), 5)), 
                                  y=list(limits=c(ymin(cropped_L20), ymax(cropped_L20)), at=seq(ymin(cropped_L20), ymax(cropped_L20), 5))),
                      colorkey=FALSE)
L22_plot <- levelplot(cropped_L22, col.regions=climate.colors, xlab="", ylab="", 
                      scales=list(x=list(limits=c(xmin(cropped_L22), xmax(cropped_L22)), at=seq(xmin(cropped_L22), xmax(cropped_L22), 5)), 
                                  y=list(limits=c(ymin(cropped_L22), ymax(cropped_L22)), at=seq(ymin(cropped_L22), ymax(cropped_L22), 5))), 
                      colorkey=FALSE)

# pdf(paste(output_directory, "/test.pdf", sep=""))
# # lattice.options(
# #   layout.heights = list(bottom.padding=list(x=0), top.padding=list(x=0)),
# #   layout.widths = list(left.padding=list(x=0), right.padding=list(x=0)))
# print(L03_plot, split=c(1,1,2,4), more=TRUE) # first 2 numbers are location of the plot (col, row), numbers 3 and 4 give the total number of cols and rows
# print(L10_plot, split=c(1,2,2,4), more=TRUE)
# print(L11_plot, split=c(1,3,2,4), more=TRUE)
# print(L18_plot, split=c(1,4,2,4), more=TRUE)
# print(L19_plot, split=c(2,1,2,4), more=TRUE)
# print(L20_plot, split=c(2,2,2,4), more=TRUE)
# print(L22_plot, split=c(2,3,2,4))
# dev.off()
